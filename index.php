<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="style.css" type="text/css" rel="stylesheet">
    <script src="jquery-1.11.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="script.js"></script>
    <title>Лифт</title>
</head>
<body>

<?php //if (!isset($stage)):?>
<!--    <script>-->
<!--       var stage = prompt('Введите количество этажей');-->
<!--    </script>-->
<?php //endif;?>
<?php //$stage = '<script>stage</script>';?>
<?php $stage = 9; ?>
<div class="home">
    <div class="elevator_left" id="elevator_left"></div>
    <div class="elevator_right" id="elevator_right"></div>
    <?php for ($i = $stage;$i > 1;$i-- ) :?>
    <div class="floor" data-id="<?=$i?>" id="floor<?=$i?>">
        <div class="left_side section">
            <div class="doors">
                <div class="door left"></div>
                <div class="door right"></div>
            </div>
        </div>
        <div class="center section">
            <div class="touch" id="touch<?=$i;?>"></div>
        </div>
        <div class="right_side section">
            <div class="doors">
                <div class="door left"></div>
                <div class="door right"></div>
            </div>
        </div>
    </div>
<?php endfor; ?>
    <div class="floor"data-id="1" id="floor1">
        <div class="left_side section">
            <div class="monitor" id="monitor_left">1</div>
            <div class="doors">
                <div class="door left" id="left"></div>
                <div class="door right" id="right"></div>
            </div>
        </div>
        <div class="center section">
            <div class="touch" id="touch1"></div>
        </div>
        <div class="right_side section">
            <div class="monitor" id="monitor_right">3</div>
            <div class="doors">
                <div class="door left"></div>
                <div class="door right"></div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        HOUSE.init();
    });
</script>
<!--sdsd-->
</body>
</html>