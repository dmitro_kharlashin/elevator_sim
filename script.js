/**
 * Created by dima on 25.09.14.
 */

var HOUSE = HOUSE || {
        init : function(){
            var self = this;
            $('.floor').each(function(){
                self.floors.push($(this));
            });
            self.initButtons();
            self.liftLeft.init();
            self.liftRight.init();
        },
        floors : [],
        buttons : [],
        initButtons : function(){
            var self = this;
            for(var i = 0; i < self.floors.length; i++){
                self.buttons.push({
                    position : self.floors[i].position(),
                    element : self.floors[i].find(".touch"),
                    floor : self.floors[i].attr("data-id"),
                    clickInit : function(j){
                        this.element.on('click', function () {
                            if (HOUSE.buttons[j].element.attr('class') !== 'touch active') {
                                var button = HOUSE.buttons[j];
                                self.dispatcher.makeLine(button);
                                button.element.attr('class', 'touch active');
                            }
                        });
                    }
                });
                self.buttons[i].clickInit(i);
            }
        },
        liftLeft : {
            init : function(){
                this.element = $("#elevator_left");
                var i = HOUSE.floors.length - 1;
                var max_y = HOUSE.floors[i].position().top +21;
                this.element.css('top', max_y + 'px');
                $('#monitor_left').html(1);
                this.availability = true;
            },
            element : null,
            availability : null,
            move : function (y, floor) {
                var self = this;

                var top= this.element.position().top;

                 var interval = setInterval(function() {
                    self.element.css('top', top + 'px');
                    if (top < y) {
                        top++;
                        self.availability = false;
                    };
                    if (top > y) {
                        top--;
                        self.availability = false;
                    };
                    if (Math.ceil(top) == Math.ceil(y)) {

                        clearInterval(interval);
			setTimeout(function(){
                        	$('#floor'+floor+' .left_side .door').animate({width : 0},2000).delay(2000);
                        	$('#floor'+floor+' .left_side .door').animate({width : 46},2000).delay(2000);
                        	$('#touch'+floor).attr('class', 'touch');
                        },500);

                        setTimeout(function(){
                            self.availability = true;
                            HOUSE.dispatcher.done();
                        },6000);
                    };
                    for (i = 0;i < HOUSE.floors.length; i++) {
                        if (top >= (HOUSE.floors[i].position().top)) {
                            $('#monitor_left').html(HOUSE.buttons[i].floor);
                        };
                    };
                },10);
            }
        },
        liftRight : {
            init : function(){
                this.element = $("#elevator_right");
                var i = HOUSE.floors.length - 1;
                var max_y = HOUSE.floors[i].position().top +21;
                this.element.css('top', max_y + 'px');
                $('#monitor_right').html(1);
                this.availability = true;
            },
            element : null,
            availability : null,
            move : function (y, floor) {
                var self = this;

                var top= this.element.position().top;

                var interval = setInterval(function() {
                    self.element.css('top', top + 'px');

                    if (top < y) {
                        top++;
                        self.availability = false;
                    };
                    if (top > y) {
                        top--;
                        self.availability = false;
                    };
                    if (Math.ceil(top) == Math.ceil(y)) {

                        clearInterval(interval);
			setTimeout(function(){
                        	$('#floor'+floor+' .right_side .door').animate({width : 0},2000).delay(2000);
                        	$('#floor'+floor+' .right_side .door').animate({width : 46},2000).delay(2000);
                        	$('#touch'+floor).attr('class', 'touch');
                        },500);

                        setTimeout(function(){
                            self.availability = true;
                            HOUSE.dispatcher.done();
                        },6000);
                    };
                    for (i = 0;i < HOUSE.floors.length; i++) {
                        if (top >= (HOUSE.floors[i].position().top)) {
                            $('#monitor_right').html(HOUSE.buttons[i].floor);
                        };
                    };
                },10);
            }
        },
        dispatcher : {
            queue : [],
            //leftPosition : null,
            //rightPosition : null,
            makeLine : function(button) {
                this.queue.push(button);
                this.router();
            },
            moveLift : function(lift) {
                    var y = this.queue[0].position.top +21;
                    var floor = this.queue[0].floor;
                this.queue.shift();
                if (lift == 'liftLeft') {
                    HOUSE.liftLeft.move(y, floor);
                }
                else if (lift == 'liftRight'){
                    HOUSE.liftRight.move(y, floor);
                };
            },
            router : function() {
                var self = this;
                    if ((HOUSE.liftLeft.availability == true) && (HOUSE.liftRight.availability == true)) {
                        var leftPosition = HOUSE.liftLeft.element.position().top;
                        var rightPosition = HOUSE.liftRight.element.position().top;
                        if (Math.abs(leftPosition-this.queue[0].position.top) < Math.abs(rightPosition-this.queue[0].position.top)) {
                            var lift = 'liftLeft';
                        }
                        else if (Math.abs(leftPosition-this.queue[0].position.top) >= Math.abs(rightPosition-this.queue[0].position.top)) {
                            var lift = 'liftRight';
                        }
                    HOUSE.dispatcher.moveLift(lift);
                }
                    else if ((HOUSE.liftLeft.availability == true) && (HOUSE.liftRight.availability == false))  {
                        var lift = 'liftLeft';
                        HOUSE.dispatcher.moveLift(lift);
                    }
                    else if ((HOUSE.liftRight.availability == true) && (HOUSE.liftLeft.availability == false)) {
                        var lift = 'liftRight';
                        HOUSE.dispatcher.moveLift(lift);
                    }
                else {
                        return;
                    }
            },
            done : function(){
                // TODO if this.queue is empty - return;
                if (this.queue.length == 0) {
                    return;
                }
                if (this.queue.length !== 0) {
                    this.router();
                };
            }
        }
    };
